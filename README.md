# cge-report

Analyses statistiques sur le devenir des jeunes diplômés de l'Ensimag.
[Gregory.Mounie AT imag.fr]

Extraction des données depuis les CSV de l'enquête emploi de la Conférence des grande écoles (Pour des raisons évidentes de confidentialité, les données ne sont pas dans ce dépot GIT, juste les scripts d'analyses)

* Situation (Travaillant dans l'industrie ou en thèse, ...)
[Situation](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/situation.org "situation")

* Secteurs (Éditeurs de logiciels, Société de service, Banques, etc.; par filière)
[Secteurs](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/secteurs.org "secteurs")

* Services (IT, R & D, etc.; par filière)
[Services](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/services.org "secteurs")

* Lieux de travail (Paris (Île de France), Auvergne-Rhone-Alpes, à l'étranger, etc.)
[Lieux](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/lieux.org "Lieux")

* Satisfactions (de la formation, de l'emploi, etc.)
[Satisfactions](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/satisfactions.org "Satisfactions")

* Salaires
[Salaires](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/salaires.org "Satisfactions")

* Répondants (Pourcentage de la promotion ayant répondu au questionnaire)
[Répondants](https://gitlab.ensimag.fr/mounieg/cge-report/blob/master/repondants.org "répondants")

# Proposez un patch
## Si vous avez un compte informatique à l'Ensimag
	1. Clone
	2. Écrire votre code
	3. envoyer un merge request
## Si vous n'avez pas, ou plus, un compte informatique à l'Ensimag
  1. Clone (public)
  2. Créer une branche locale
  2. Écrire votre joli code dans la branche (avec des messages de commit explicites)
  3. Vérifier que votre branche master est à jour, et si besoin, faire un rebase de la branche
  ``` shell
  git pull
  git rebase master # si la branche master a changé
  ```
  4. Créer un patch de la différence avec master
  ``` shell
  git format-patch master
  ```
  5. Envoyer moi le fichier mbox/patch attaché à un email pour Gregory.Mounie AT imag.fr
  

